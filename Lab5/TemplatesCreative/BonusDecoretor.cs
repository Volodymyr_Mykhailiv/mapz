﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplatesCreative
{
    public abstract class Bonus
    {
        public int Supplies { get; set; }

        public int Balance { get; set; } = 0;
       
        public abstract void CalculateBonus();
    }

    class SimpleBonus : Bonus
    {
        //public int Balance { get; private set; }
        public SimpleBonus() { }
        public override void CalculateBonus()
        {
            Balance += 10;
        }
    }
        
    public abstract class BonusDecorator: Bonus
    {
        protected Bonus? _bonus;

        public BonusDecorator() 
        { 

        }
        public  BonusDecorator(Bonus bonus)
        {
            _bonus = bonus;
        }
        public override void CalculateBonus() 
        { 
            if(_bonus != null)
            {
                _bonus.CalculateBonus();
            }
        }
    }

    public class ImprovedBonus : BonusDecorator 
    {
       
        public ImprovedBonus() : base() { }
        public ImprovedBonus(Bonus bonus) : base(bonus) { }
        public void ImproveBoat()
        {
            Supplies += 5;
        }
        public override void CalculateBonus()
        {
            base.CalculateBonus();
            ImproveBoat();
        }
    }
}
