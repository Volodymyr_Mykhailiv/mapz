﻿using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TemplatesCreative;

namespace TemplatesCreative
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Point> allPositions = new List<Point>();
        private List<Point> freePositions = new List<Point>();
        public MainWindow()
        {

            InitializeComponent();
            InitializePositions(canvas, 100, 100);
           

            Loaded += MainWindow_Loaded;
            PreviewKeyDown += Window_PreviewKeyDown; // Додати обробник 
        }
        private void InitializePositions(Canvas canvas, double skyImageWidth, double skyImageHeight)
        {
            if (allPositions.Count == 0)
            {
                // Коригуємо обчислення кількості рядків і стовпців
                int rows = (int)(750 / skyImageHeight);
                int cols = (int)(700 / skyImageWidth);

                for (int row = 2; row < rows; row++)
                {
                    for (int col = 0; col < cols; col++)
                    {
                        double left = col * skyImageWidth;
                        double top = row * skyImageHeight;
                        Point position = new Point(left, top);
                        allPositions.Add(position);
                        freePositions.Add(position);
                    }
                }
            }
        }


        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            // Викликаємо метод InitializeGameMap, передаючи контейнер Canvas для розміщення зображень
            GameMap.Instance.InitializeGameMap(canvas);
        }
        private IShipFactory GalleonFactory = new Galleon();
        private IShipFactory FrigateFactory = new Frigate();

        public Image ShipImage;
        private Treasure treasure;
        private ShipMovement movement;
        private IBoat galleonBoat;
        private IBoat frigateBoat;
        private Bonus bonus;
        private void CreateGalleon_Click(object sender, RoutedEventArgs e)
        {
            
            treasure = new Treasure(10);
            treasure.Generate(canvas, ref allPositions, ref freePositions);

            /////////////
            if (galleonBoat == null)
            {
                galleonBoat = GalleonFactory.CreateBoat();
            }

            galleonBoat.Sail(canvas, ref allPositions, ref freePositions);
            ShipImage = galleonBoat.GetShipImage(); // Отримуємо зображення
            movement = new ShipMovement(canvas, ShipImage);

            //////////
            bonus = new SimpleBonus();
        }

        private void CreateFrigate_Click(object sender, RoutedEventArgs e)
        {
            treasure = new Treasure(10);
            treasure.Generate(canvas, ref allPositions, ref freePositions);

            //////////////
            if (frigateBoat == null)
            {
                frigateBoat = FrigateFactory.CreateBoat();
            }

            frigateBoat.Sail(canvas, ref allPositions, ref freePositions);
            ShipImage = frigateBoat.GetShipImage(); // Отримуємо зображення
            movement = new ShipMovement(canvas, ShipImage);

            //////////
            bonus = new SimpleBonus();

        }

        public static List<Rank> Ranks = new()
       {
           new Rank(PirateRank.Captain),
           new Rank(PirateRank.Quarternaster),
           new Rank(PirateRank.Boatswain),
           new Rank(PirateRank.Navigator),
           new Rank(PirateRank.CabinBoy),
           new Rank(PirateRank.Cook),
           new Rank("Cannoneer")
      };

        private void ShowInfoButton_Click(object sender, RoutedEventArgs e)
        {
            var Pirates = new List<IPirateComponent>
            {
                new PirateLeaf("Anne Bonny", Ranks[5], 13, "Frigate"),
                new PirateLeaf("Calico Jack ", Ranks[1], 19, "Frigate"),
                new PirateLeaf("Blackbeard", Ranks[0], 99, "Galleon"),
                new PirateLeaf("William Kidd", Ranks[3], 18, "Galleon"),
                new PirateLeaf("Jean Lafitte", Ranks[2], 33, "Galleon")
            };

            PirateBranch FrigateBranch = new PirateBranch();
            FrigateBranch.AddComponent(Pirates[0]);
            FrigateBranch.AddComponent(Pirates[1]);
            PirateBranch GalleonBranch = new PirateBranch();
            GalleonBranch.AddComponent(Pirates[2]);
            GalleonBranch.AddComponent(Pirates[3]);
            GalleonBranch.AddComponent(Pirates[4]);
            GalleonBranch.AddComponent(FrigateBranch);
            // InfoTextBlock.Text = tree.ShowInfo();
            MessageBox.Show(GalleonBranch.ShowInfo());
        }


        //private int Balance = 0;

        private Bonus improveBonus;
        public void CheckPositions()
        {
            Point ShipPoint = movement.GetShipCurrentPossition();
            Point TreasurePoint = treasure.GetTreasurePosition();
            if (ShipPoint.X == TreasurePoint.X && ShipPoint.Y == TreasurePoint.Y)
            { 
                if(bonus.Balance % 50 == 0 && bonus.Balance != 0)
                {
                    if (improveBonus == null) // Перевіряємо, чи є вже створений об'єкт improveBonus
                    {
                        improveBonus = new ImprovedBonus(bonus); // Якщо об'єкт ще не створений, то створюємо його
                    }
                   
                    improveBonus.CalculateBonus(); // Виконуємо обчислення покращеного бонусу
                  
                    SuppliesTextBlock.Text = improveBonus.Supplies.ToString(); 
                }
                else
                {
                    bonus.CalculateBonus();
                }
                treasure.ChangeTreasurePosition();
                BalanceTextBlock.Text = bonus.Balance.ToString();


            }
        }

        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Up:
                    movement.MoveUp();
                    CheckPositions();
                    break;
                case Key.Down:
                   movement.MoveDown();
                    CheckPositions();
                    break;
                case Key.Left:
                   movement.MoveLeft();
                    CheckPositions();
                    break;
                case Key.Right:
                   movement.MoveRight();
                    CheckPositions();
                    break;
                default:
                    break;
            }
        }
    }
}


