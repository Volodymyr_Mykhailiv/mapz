﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows;

namespace TemplatesCreative
{
    public class Frigate : IShipFactory
    {
        public IBoat CreateBoat()
        {
            return new FrigateBoat();
        }

        public ICrew CreateCrew()
        {
            return new FigateCrew();
        }

        public IWeapon CreateWeapon()
        {
            return new FigateWeapon();
        }

        public ISupplies CreateSupplies()
        {
            return new FigateSupplies();
        }
    }




    // конкретні компоненти типу корабля


    public class FrigateBoat : IBoat
    {
        //public void Sail(Canvas canvas, ref List<Point> allPositions, ref List<Point> freePositions)
        //{


        //    string frigatImagePath = ".\\Images\\frigat.png";
        //    BitmapImage frigatBitmap = new BitmapImage(new Uri(frigatImagePath, UriKind.Relative));

        //    double frigatImageWidth = 100; // Ширина зображення рівна ширині Canvas
        //    double frigatImageHeight = 100; // Висота зображення "sky"

        //    // Випадковим чином перемішати список вільних позицій
        //    Random random = new Random();
        //    freePositions = freePositions.OrderBy(p => random.Next()).ToList();

        //    // Список зайнятих позицій
        //    List<Point> occupiedPositions = new List<Point>();

        //    // Проходження по перемішаному списку позицій і вставка зображення в перший вільний сектор
        //    List<Point> positionsToRemove = new List<Point>(); // Список позицій для видалення

        //    foreach (Point position in freePositions)
        //    {
        //        // Перевірка, чи позиція вільна
        //        bool positionOccupied = occupiedPositions.Any(p =>
        //            Math.Abs(p.X - position.X) < frigatImageWidth &&
        //            Math.Abs(p.Y - position.Y) < frigatImageHeight);

        //        if (!positionOccupied)
        //        {

        //            Image frigatImage = new Image();
        //            frigatImage.Source = frigatBitmap;
        //            frigatImage.Width = frigatImageWidth;
        //            frigatImage.Height = frigatImageHeight;
        //            Canvas.SetLeft(frigatImage, position.X);
        //            Canvas.SetTop(frigatImage, position.Y);
        //            canvas.Children.Add(frigatImage);

        //            // Додавання позиції до списку зайнятих позицій
        //            occupiedPositions.Add(position);

        //            positionsToRemove.Add(position); // Додати позицію до списку для видалення

        //            break; // Вставлено зображення, виходимо з циклу
        //        }
        //    }

        //    // Видалення зайнятих позицій зі списку вільних позицій
        //    foreach (var pos in positionsToRemove)
        //    {
        //        freePositions.Remove(pos);
        //    }
        //    if (freePositions.Count == 0)
        //    {
        //        MessageBox.Show("All poitions are occupied");
        //    }
        //}

        public int StartX = 0;
        public int StartY = 200;
        public Image FrigateImage;
        public void Sail(Canvas canvas, ref List<Point> allPositions, ref List<Point> freePositions)
        {
            // Шлях до зображення "Galleon"


            string frigateImagePath = ".\\Images\\frigat.png";
            BitmapImage galleonBitmap = new BitmapImage(new Uri(frigateImagePath, UriKind.Relative));

            // Розмір зображення "sky"
            double frigateImageWidth = 100; // Ширина зображення рівна ширині Canvas
            double frigateImageHeight = 100;

            // Початкові координати корабля

            // Створення Image для "Galleon"
            Image frigateImage = new Image();
            frigateImage.Source = galleonBitmap;
            frigateImage.Width = frigateImageWidth;
            frigateImage.Height = frigateImageHeight;
            Canvas.SetLeft(frigateImage, StartX);
            Canvas.SetTop(frigateImage, StartY);
           
            canvas.Children.Add(frigateImage);
            FrigateImage = frigateImage;

        }
        public Image GetShipImage()
        {
            return FrigateImage;
        }
        public void Navigate()
        {
            Console.WriteLine("Go to west");
        }
    }


    public class FigateCrew : ICrew
    {
        public void AssignRole()
        {
            Console.WriteLine("You are capitan");
        }

       public void AddCrewMember()
        {
            Console.WriteLine("new member");
        }

       public void RemoveCrewMember()
        {
            Console.WriteLine("sorry you were dalited");
        }
    }

    public class FigateWeapon : IWeapon
    {
       public void Fire()
       {
            Console.WriteLine("тидищ");
       }
       public void Reload()
        {
            Console.WriteLine("reload weapon");
        }
    }

    public class FigateSupplies : ISupplies
    {
        public void ConsumeSupply() 
        {
            Console.WriteLine("Am am am am aaaam am");
        }
        public int CheckSupplyLevel()
        {
            return 10;
        }
        public void AddSupply()
        {
            Console.WriteLine("Add bannanas");
        }

    }
}
