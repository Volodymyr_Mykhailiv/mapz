﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplatesCreative
{
  public interface IMapElement
    {
        IMapElement Clone();
    }

    public class Obstacle : IMapElement
    {
        public string Type { get; set; }
        public int Size { get; set; } 

        public Obstacle(string type, int size)
        {
            Type = type;
            Size = size;
        }

        public IMapElement Clone()
        {
            return new Obstacle(Type, Size);
        }
    }

    public class Treasure : IMapElement
    {
        public int Value { get; set; }
        public string Description { get; set; } 

        public Treasure(int value, string description)
        {
            Value = value;
            Description = description;
        }

        public IMapElement Clone()
        {
            return new Treasure(Value, Description);
        }
    }

}
