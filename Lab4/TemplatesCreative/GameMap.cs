﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using static System.Net.Mime.MediaTypeNames;

namespace TemplatesCreative
{
    public class GameMap
    {
        public int Width { get; set; }
        public int Height { get; set; }

        private static GameMap instance;
        public static GameMap Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GameMap();
                }
                return instance;
            }
        }
        
        
        private GameMap()   
        {
            Width = 800;
            Height = 600;
        }
        
        public void SetMapSize(string windowName, int width, int height)
        {
            Window targetWindow = System.Windows.Application.Current.Windows.OfType<Window>().FirstOrDefault(w => w.Name == windowName);
            if (targetWindow != null)
            {
                targetWindow.Width = width;
                targetWindow.Height = height;
            }
        }

       


        public void InitializeGameMap(Canvas canvas)
        {
            // Очищення контейнера Canvas перед додаванням нових елементів
            canvas.Children.Clear();

            // Шлях до зображення "sky"
             //string skyImagePath = @"C:\Users\Volodymyr_Mykhailiv\OneDrive\Documents\універ 4 семестр\MAPZ\Lab4\TemplatesCreative\Images\takan.png";
            //  string skyImagePath = "pack://application:,,,/Images/takan.png";
            string skyImagePath = ".\\Images\\takan.png";
            BitmapImage skyBitmap = new BitmapImage(new Uri(skyImagePath, UriKind.Relative));

            // Розмір зображення "sky"
            double skyImageWidth = canvas.ActualWidth; // Ширина зображення рівна ширині Canvas
            double skyImageHeight = 200; // Висота зображення "sky"

            // Створення Image для "sky"
            System.Windows.Controls.Image skyImage = new System.Windows.Controls.Image();
            skyImage.Source = skyBitmap;
            skyImage.Width = skyImageWidth;
            skyImage.Height = skyImageHeight;
            Canvas.SetLeft(skyImage, 0);
            Canvas.SetTop(skyImage, 0);
            canvas.Children.Add(skyImage);

            // Шлях до зображення "ocean"
       
            string oceanImagePath = ".\\Images\\water2.png";
            BitmapImage oceanBitmap = new BitmapImage(new Uri(oceanImagePath, UriKind.Relative));
          

            // Розмір зображення "ocean"
            double oceanImageWidth = 100;
            double oceanImageHeight = 100;

            // Висота канвасу без "sky"
            double canvasHeightWithoutSky = canvas.ActualHeight - skyImageHeight;

            // Кількість рядків зображень "ocean"
            int rowCount = (int)Math.Ceiling(canvasHeightWithoutSky / oceanImageHeight);

            // Проходження по кожному рядку для додавання зображень "ocean"
            for (int row = 0; row < rowCount; row++)
            {
                // Проходження по ширині канвасу для кожного рядка
                for (double left = 0; left < canvas.ActualWidth; left += oceanImageWidth)
                {
                    System.Windows.Controls.Image oceanImage = new System.Windows.Controls.Image();
                    oceanImage.Source = oceanBitmap;
                    oceanImage.Width = oceanImageWidth;
                    oceanImage.Height = oceanImageHeight;
                    Canvas.SetLeft(oceanImage, left);
                    Canvas.SetTop(oceanImage, skyImageHeight-2 + row * oceanImageHeight); // Починаємо від "sky" і кожен наступний рядок - вниз
                    canvas.Children.Add(oceanImage);
                }
            }
        }

        public void UpdateMap() 
        {
            throw new NotImplementedException();
        }

        public void ClearMap()
        {
            throw new NotImplementedException();
        }

        public void RenderPirates()
        {
            throw new NotImplementedException();
        }
        public void RenderBoats()
        {
            throw new NotImplementedException();
        }
    }
}
