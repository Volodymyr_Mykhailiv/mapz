﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplatesCreative
{
    public class Pirate
    {
        public Guid Id { get; set; }
        public string Name { get; private set; }
        public int Money { get; set; }
        public Guid RankId { get; private set; }
        public string RankName { get; private set; }

        public readonly bool IsCaptain;
        public Pirate(string name, Rank rank)
        {
            Id = Guid.NewGuid();
            Name = name;
            RankId = rank.Id;
            RankName = rank.RankName;
            Money = 0;
            IsCaptain = rank.RankName == PirateRank.Captain.ToString();
        }
    }

    public class Rank
    {
        public Guid Id { get; private set; }
        public string RankName { get; private set; }
        public Rank(PirateRank rank)
        {
            RankName = rank.ToString();
            Id = Guid.NewGuid();
        }
        public Rank(string name)
        {
            RankName = name;
            Id = Guid.NewGuid();
        }
    }

    public enum PirateRank
    {
        Captain,
        Quarternaster,
        Boatswain,
        Navigator,
        CabinBoy,
        Cook
    }
}
