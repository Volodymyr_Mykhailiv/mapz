﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplatesCreative
{
       interface IShipFactory
        {
            IBoat CreateBoat();
            ICrew CreateCrew();
            IWeapon CreateWeapon();
            ISupplies CreateSupplies();
        }
}
