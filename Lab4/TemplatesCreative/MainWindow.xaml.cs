﻿using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TemplatesCreative
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Point> allPositions = new List<Point>();
        private List<Point> freePositions = new List<Point>();
        public MainWindow()
        {
            InitializeComponent();
            InitializePositions(canvas, 100, 100);
           

            Loaded += MainWindow_Loaded;
        }
        private void InitializePositions(Canvas canvas, double skyImageWidth, double skyImageHeight)
        {
            if (allPositions.Count == 0)
            {
                // Коригуємо обчислення кількості рядків і стовпців
                int rows = (int)(750 / skyImageHeight);
                int cols = (int)(700 / skyImageWidth);

                for (int row = 2; row < rows; row++)
                {
                    for (int col = 0; col < cols; col++)
                    {
                        double left = col * skyImageWidth;
                        double top = row * skyImageHeight;
                        Point position = new Point(left, top);
                        allPositions.Add(position);
                        freePositions.Add(position);
                    }
                }
            }
        }


        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            // Викликаємо метод InitializeGameMap, передаючи контейнер Canvas для розміщення зображень
            GameMap.Instance.InitializeGameMap(canvas);
        }
        private IShipFactory GalleonFactory = new Galleon();
        private IShipFactory FrigateFactory = new Frigate();
        private void CreateGalleon_Click(object sender, RoutedEventArgs e)
        {
            GalleonFactory.CreateBoat().Sail(canvas, ref allPositions, ref freePositions);
        }

        private void CreateFrigate_Click(object sender, RoutedEventArgs e)
        {

            FrigateFactory.CreateBoat().Sail(canvas, ref allPositions, ref freePositions);
        }
    }
}