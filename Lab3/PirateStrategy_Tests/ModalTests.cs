using FluentAssertions;
using PiratStrategy;
using System.Linq;
using System.Net.WebSockets;
using System.Text;

namespace PirateStrategy_Tests
{
    public static class ModalTests
    {
        public static readonly List<Rank> Ranks = new()
       {
           new Rank(PirateRank.Captain),
           new Rank(PirateRank.Quarternaster),
           new Rank(PirateRank.Boatswain),
           new Rank(PirateRank.Navigator),
           new Rank(PirateRank.CabinBoy),
           new Rank(PirateRank.Cook),
           new Rank("Cannoneer")
       };

        public static Guid GetSpeciesId(PirateRank name)
        {
            return Ranks.Find(el => el.RankName == name.ToString())?.Id ?? Guid.Empty;
        }

        public static List<Pirate> Pirates =>
       new()
       {
           new Pirate("Anne Bonny", Ranks[5]) { Money = 58 },
           new Pirate("Calico Jack ", Ranks[1]) { Money = 19 },
           new Pirate("Blackbeard", Ranks[0]) { Money = 99 },
           new Pirate("Henry Morgan", Ranks[4]) { Money = 14 },
           new Pirate("Black Bart ", Ranks[6]) { Money = 75 },
           new Pirate("Black Sam Bellamy", Ranks[2]) { Money = 76 },
           new Pirate("Mary Read", Ranks[5]) { Money = 68 },
           new Pirate("Edward Low", Ranks[1]) { Money = 21 },
           new Pirate("Captain Kidd", Ranks[0]) { Money = 65 },
           new Pirate("Bartholomew Roberts", Ranks[3]) { Money = 50 },
           new Pirate("William Kidd", Ranks[3]) { Money = 18 },
           new Pirate("Jean Lafitte", Ranks[2]) { Money = 33 },
       };

        //public static void PrintPirates()
        //{
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append(String.Format(" {0,-25} {1,-40} {2,-18} {3,-10} {4,-10}\n", "Name", "Rank ID", "Rank Name", "Money", "IsCaptain"));

        //    Pirates.ForEach(pirate => sb.Append(String.Format(" {0,-25} {1,-40} {2,-18} {3,-10} {4,-12}\n",
        //     pirate.Name,
        //     pirate.RankId.ToString(),
        //     Ranks.Find(rank => rank.Id == pirate.RankId)?.RankName.ToString() ?? "Unknown",
        //     pirate.Money.ToString(),
        //     pirate.IsCaptain.ToString())));

        //    Console.WriteLine(sb.ToString());
        //}

        [Fact]
        public static void TestPiratesToDictionary()
        {
            //Arange

            //Act
            var PiratesDict = Pirates.ToDictionary(
                pirate => pirate.Name, 
                pirate => pirate);
            //Assert
            Assert.IsType<Dictionary<string, Pirate>>(PiratesDict);
            PiratesDict.Keys.Should().AllBeOfType<string>();

            var res = PiratesDict.Where(pirate => pirate.Value.IsCaptain);
            Assert.Equal(2, res.Count());
        }

        [Fact]
        public static void TestPiratesToQueue()
        {
            var PirateQeue = new Queue<Pirate>(Pirates);
            Assert.Equal("Anne Bonny", PirateQeue.Peek().Name);

            PirateQeue.Enqueue(new Pirate("Captain Jack", Ranks[0]) { Money = 30 });
            PirateQeue.Count.Should().Be(Pirates.Count + 1);
        }

        [Fact]
        public static void TestPiratesToStack()
        {
            var PirateStack = new Stack<Pirate>(Pirates);
            Assert.Equal("Jean Lafitte", PirateStack.Peek().Name);

            PirateStack.Clear();
            PirateStack.Count.Should().Be(0);
        }

        [Fact]
        public static void TestPiratesToHashSet()
        {
            var PirateHashSet = new HashSet<Pirate>(Pirates);
            Assert.Equal(PirateHashSet.Count, Pirates.Count);

            PirateHashSet.Clear();
            PirateHashSet.Count.Should().Be(0);
        }



        [Fact]
        public static void TestPiratesToLookUp()
        {
            var PirateLookup = Pirates.ToLookup(pirate =>  pirate.RankId, pirate => pirate);
            Assert.Equal(2, PirateLookup[GetSpeciesId(PirateRank.Captain)].Count());
        }

        [Fact]
        public static void TestPiratesGroupBy()
        {
            var PiratesGroupedByRank = Pirates.GroupBy(pirate => pirate.RankName);
            Assert.Equal(7, PiratesGroupedByRank.Count());
            
        }

        [Fact]
        public static void TestPiratesOrderBy()
        {
            var PiratesOrderBy = Pirates.OrderBy(pirate => pirate.Money);
            Assert.Equal(14, PiratesOrderBy.First().Money);

            var PiratesOrderByDescending = Pirates.OrderByDescending(pirate => pirate.Money);
            Assert.Equal(99, PiratesOrderByDescending.First().Money);
        }


        [Fact]
        public static void TestExtansionMtthod()
        {
            var PiratesToSortedKist = Pirates.ToSortedList(pirate => pirate.Name);
            PiratesToSortedKist.Keys.Should().BeInAscendingOrder();
        }

        [Fact]
        public static void ComplexLinqQuery()
        {
           var SmallestMoneyInCaptain = Pirates.Where(pirate => pirate.IsCaptain)
                .Select(pirate => pirate.Money).OrderByDescending(money => money);
           
           SmallestMoneyInCaptain.Last().Should().Be(65);



            var queryResult = Pirates.Where(pirate => pirate.Name.Contains("Black"))
                .GroupBy(pirate => pirate.RankId)
                .Select(group => group.Max())
                .Where(res => res != null)
                .OrderBy(res => res?.Money)
                .ToList();
            queryResult.Should().NotBeNull().And.HaveCount(3);
            queryResult.Should().ContainSingle(pirate => pirate.Name == "Blackbeard");
            
        }

        [Fact]
        public static void TestConvertToArray()
        {
            var array = Pirates.Select(pirate => pirate.Money).ToArray();
            array.Should().OnlyContain(money => money >= 0 && money <= 100);
        }

        [Fact]
        public static void TestAnonimys() 
        {
            var SomePirates = Pirates.Select(pirate =>  new { pirate.Name, pirate.RankId });
            Assert.Equal(SomePirates.Count(), Pirates.Count);
        }

        [Fact]
        public static void TestSortByName() 
        {
            var PiratesToSortByName = Pirates.OrderBy(pirate => pirate, new PirateComparer());
            Assert.Equal("Anne Bonny", PiratesToSortByName.First().Name);
        }
    }








    
}