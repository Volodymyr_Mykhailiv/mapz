﻿using PiratStrategy;
using System;
using System.Text;

namespace MyApp
{
    internal class Program
    {


        public static readonly List<Rank> Ranks = new()
       {
           new Rank(PirateRank.Captain),
           new Rank(PirateRank.Quarternaster),
           new Rank(PirateRank.Boatswain),
           new Rank(PirateRank.Navigator),
           new Rank(PirateRank.CabinBoy),
           new Rank(PirateRank.Cook),
           new Rank("Cannoneer")
       };

        public static List<Pirate> Pirates =>
       new()
       {
           new Pirate("Anne Bonny", Ranks[5]) { Money = 58 },
           new Pirate("Calico Jack ", Ranks[1]) { Money = 19 },
           new Pirate("Blackbeard", Ranks[0]) { Money = 99 },
           new Pirate("Henry Morgan", Ranks[4]) { Money = 14 },
           new Pirate("Black Bart ", Ranks[6]) { Money = 75 },
           new Pirate("Black Sam Bellamy", Ranks[2]) { Money = 76 },
           new Pirate("Mary Read", Ranks[5]) { Money = 68 },
           new Pirate("Edward Low", Ranks[1]) { Money = 21 },
           new Pirate("Captain Kidd", Ranks[0]) { Money = 65 },
           new Pirate("Bartholomew Roberts", Ranks[3]) { Money = 50 },
           new Pirate("William Kidd", Ranks[3]) { Money = 18 },
           new Pirate("Jean Lafitte", Ranks[2]) { Money = 33 },
       };

        public static void PrintPirates()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(String.Format(" {0,-25} {1,-40} {2,-18} {3,-10} {4,-10}\n", "Name", "Rank ID", "Rank Name", "Money", "IsCaptain"));

            Pirates.ForEach(pirate => sb.Append(String.Format(" {0,-25} {1,-40} {2,-18} {3,-10} {4,-12}\n",
             pirate.Name,
             pirate.RankId.ToString(),
             Ranks.Find(rank => rank.Id == pirate.RankId)?.RankName.ToString() ?? "Unknown",
             pirate.Money.ToString(),
             pirate.IsCaptain.ToString())));

            Console.WriteLine(sb.ToString());
        }



        static void Main(string[] args)
        {
            PrintPirates();
           // Console.WriteLine("Hello World!");
        }

    }
}