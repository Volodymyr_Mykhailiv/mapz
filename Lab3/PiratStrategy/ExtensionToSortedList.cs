﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PiratStrategy
{
   public static class ExtensionToSortedList
    {  
        public static SortedList<K, T> ToSortedList<T, K>(this List<T> list, Func<T, K> keySelector) where K : notnull
        {
            var sortedList = new SortedList<K, T>();
            list.ForEach(el => sortedList.Add(keySelector(el), el));
            return sortedList;
        }
    }



    public class PirateComparer : IComparer<Pirate>
    {
        public int Compare(Pirate? x, Pirate? y)
        {
            return string.Compare(x?.Name, y?.Name, StringComparison.Ordinal);
        }
    }
}
