﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab2_task_11;
//using Lab2_task_2;
using Lab2_task_8;

namespace Lab2
{

    public enum Days
    {
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Sunday
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            MyClass Class = new MyClass();
            MyClass Class2 = new MyClass();
            




            int num = 1;
            object obj = num; // boxing
            int res = (int)obj; // unboxing


            Digit digit1 = new Digit(5);
            // явний оператор приведення
            int value1 = (int)digit1;

            Console.WriteLine(value1);
            int value2 = 10;
            // неявний оператор приведення
            Digit digit2 = value2;

            Console.WriteLine(digit2.Value);


            //Days day = Days.Monday;
            //if ((day & Days.Sunday) == Days.Sunday)
            //    Console.WriteLine("Day is Sunday");
            //else
            //    Console.WriteLine("Day is not Sunday");
            //if ((day | Days.Wednesday) == Days.Wednesday)
            //    Console.WriteLine("Day is Wednesday");
            //else
            //    Console.WriteLine("Day is not Wednesday");
            //if ((day ^ Days.Thursday) == Days.Monday)
            //    Console.WriteLine("Day is not Thursday");
            //else
            //    Console.WriteLine("Day is Thursday");
            //if (day == Days.Monday && day != Days.Friday)
            //    Console.WriteLine("Day is Monday");
            //else
            //    Console.WriteLine("Error");
            //if (day == Days.Monday || day == Days.Saturday)
            //    Console.WriteLine("Day is Monday or Saturday");
            //else
            //    Console.WriteLine("Error");






            //Person person = new Person("Oleh", 4638277, 18);

            //person.Age = 12;
            //person.Id = 832737289;
            //person.Name = "Petro";

            //int Iden = person.GetID();

            //person.SayAge();
            //person.SayName();
        }
    }
}
