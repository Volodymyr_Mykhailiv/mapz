﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    public struct  SmallStruct
    {
        public int x;
    }

    public struct BigStruct
    {
        public int intX;
        public double doubleX;
        public string stringX;
        public decimal decimalX;
        public bool boolX;
    }

    public class SmallClass
    {
        public int x;
    }

    public class BigClass
    {
        public int intX;
        public double doubleX;
        public string stringX;
        public decimal decimalX;
        public bool boolX;
    }
    public static class Test_Ref_Out
    {
        public static void IntRef(ref int x)
        {
            x++;
        }

        public static void IntOut(out int x)
        {
            x = 0;
        }
        public static void IntWithoutRef(int x)
        {
            x++;
        }


        public static void SmallStructRef(ref SmallStruct smallStruct)
        {
            smallStruct.x++;
        }
        public static void SmallStructOut(out SmallStruct smallStruct)
        {
            smallStruct = new SmallStruct();
        }

        public static void SmallStructWithoutRef( SmallStruct smallStruct)
        {
            smallStruct.x++;
        }


        public static void BigStructRef(ref BigStruct bigStruct)
        {
            bigStruct.intX++;
            bigStruct.doubleX++;
            bigStruct.stringX = "Some message";
            bigStruct.decimalX++;
            bigStruct.boolX = true;
        }
        public static void BigStructOut(out BigStruct bigStruct)
        {
            bigStruct = new BigStruct();
        }

        public static void BigStructWithoutRef(BigStruct bigStruct)
        {
            bigStruct.intX++;
            bigStruct.doubleX++;
            bigStruct.stringX = "Some message";
            bigStruct.decimalX++;
            bigStruct.boolX = true;
        }

        public static void SmallClassRef(ref SmallClass smallClass)
        {
            smallClass.x++;
        }
        public static void SmallClassOut(out SmallClass smallClass)
        {
            smallClass = new SmallClass();
        }

        public static void SmallClassWithoutRef(SmallClass smallClass)
        {
            smallClass.x++;
        }


        public static void BigClassRef(ref BigClass bigClass)
        {
            bigClass.intX++;
            bigClass.doubleX++;
            bigClass.stringX = "Some message";
            bigClass.decimalX++;
            bigClass.boolX = true;
        }
        public static void BigClassOut(out BigClass bigClass)
        {
            bigClass = new BigClass();
        }

        public static void BigClassWithoutRef(BigClass bigClass)
        {
            bigClass.intX++;
            bigClass.doubleX++;
            bigClass.stringX = "Some message";
            bigClass.decimalX++;
            bigClass.boolX = true;
        }
    }
}
