﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_task_11
{

    class Digit
    {
        public int Value { get; set; }
        public Digit(int value)
        {
            Value = value;
        }
        // Явний оператор приведення від Digit до int
        public static explicit operator int(Digit digit)
        {
            return digit.Value;
        }
        // Неявний оператор приведення від int до Digit
        public static implicit operator Digit(int value)
        {
            return new Digit(value);
        }
    }

    //Digit digit1 = new Digit(5);
    //// явний оператор приведення
    //int value1 = (int)digit1;

    //Console.WriteLine(value1);
    //int value2 = 10;
    //// неявний оператор приведення
    //Digit digit2 = value2;

    //Console.WriteLine(digit2.Value);





    public class ClassObject
    {
        private int value;

        public ClassObject(int value)
        {
            this.value = value;
        }

        public override string ToString()
        {
            return "Value: " + value;
        }

        public override int GetHashCode()
        {
            return value.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if(obj == null || !(obj is ClassObject)) 
            {
                return false;
            }
            ClassObject other = (ClassObject)obj;
            return value == other.value;
        }

        // Захищений метод, який викликається перед знищенням об'єкта.
        protected virtual void Finalize()
        {
        }

    }
}
