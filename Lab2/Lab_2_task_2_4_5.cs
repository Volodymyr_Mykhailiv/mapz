﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_task_2
{
   
    class Person
    {
        public string Name { get; set; }
        protected int Id { get; set; }
        private int Age { get; set; }

        public Person(string name, int id, int age)
        {
            Name = name;
            Id = id;
            Age = age;
        }

        private int GetID()
        {
            return Id;
        }

        public void SayName()
        {
            Console.WriteLine($"My name is {Name}");
        }

        protected void SayAge()
        {
            Console.WriteLine($"I am {Age} years old");
        }
       
    }

    class Student : Person
    {
        public Student(string name, int id, int age) : base(name, id, age) { }

        public void SomeInfo()
        {
            string name = Name;
            int Id = base.Id;
            //int Age = base.Age;

            SayAge();
            SayName();
            //int Iden = GetID();

        }
      
    }


    public class OuterClass
    {
        private class InnerClass
        {
            private int innerField;
        }
       
    }


    

    // множинне наслідування

    public interface IInterface1
    {
        void DoSomething1();
    }

    public interface IInterface2
    {
        void DoSomething2();
    }

    public class BaseClass
    {
       public void Print(string massage)
        {
            Console.WriteLine(massage);
        } 
    }

    public class MyClass: BaseClass, IInterface1, IInterface2
    {
        public void DoSomething1()
        {
            Console.WriteLine("From IInterface1");
        }
        public void DoSomething2()
        {
            Console.WriteLine("From IInterface2");
        }


    }
}
