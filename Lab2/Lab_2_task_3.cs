﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_task_3
{
    struct Point
    {
         double _X;
         double _Y;
        Point(double x, double y)
        {
            _X = x;
            _Y = y;
        }

        void Print()
        {
            Console.WriteLine($"X = {_X}\n Y = {_Y}");
        }
    }

   interface IStudy
    {
        void Study(string Subject);
    }


    class Person
    {
        string Name { get; set; }
        int _age;
        int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        void Print()
        {
            Console.WriteLine($"My name is {Name}, and I am {_age} years old");
        }

        class Address
        {
             string Street { get; set; }
             string City { get; set; }
             string ZipCode { get; set; }

             Address(string street, string city, string zipCode)
            {
                Street = street;
                City = city;
                ZipCode = zipCode;
            }
        }

    }   

}
