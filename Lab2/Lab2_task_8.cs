﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_task_8
{
    public class MyClass
    {
        private static int staticField = 5;
        private int field;
      
        public MyClass()
        {
            Console.WriteLine("Constructor");
            field = InitField();
        }

        static MyClass()
        {
            Console.WriteLine("Static constructor");
            staticField = InitStaticField();
        }
        private static int InitStaticField()
        {
            Console.WriteLine("Initializing static field");
            return 0;
        }
        private int InitField()
        {
            Console.WriteLine("Initializing field");
            return 0;
        }





        public void Add(int a, int b, out int res)
        {
            res = a + b;
        }

        public void swap<T>(ref T a, ref T b)
        {
            T temp = a;
            a = b;
            b = temp;
        }


        // Коли ref та out не мають значння
        //public void Print(out int a)
        //{
        //    Console.WriteLine(a);
        //}

        public void ChangeField(ref MyClass myClass)
        {
            myClass.field = 9;
        }
    }

}
