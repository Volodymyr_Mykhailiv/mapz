﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_task_1
{
   interface IGo
    {
        void Go();
    }


   
    abstract class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public Person(string name, int age)
        {
            Name = name;
            Age = age; 
        }
        public abstract void Work();

        public void PrintInfo()
        {
            Console.WriteLine(Name);
            Console.WriteLine(Age);
        }
    }

    class Teacher : Person, IGo
    {
        public string Subject { get; set; }
        public Teacher(string name, int age, string subject):base(name, age) 
        { 
            Subject = subject;
        }

        public Teacher(): this("Anna", 38, "English") { }

        public void PrintAllInfo()
        {
            base.PrintInfo();
            Console.WriteLine(Subject);
        }

        public void ShowInfo(Teacher teacher)
        {
            Console.WriteLine(teacher.Name);
            Console.WriteLine(teacher.Age);
            Console.WriteLine(teacher.Subject);
        }

        public void ShowInfo()
        {
            Console.WriteLine(Name);
            Console.WriteLine(Age);
            Console.WriteLine(Subject);
        }

        public void Go()
        {
            Console.WriteLine("Teacher go");
        }

        public override void Work()
        {
            Console.WriteLine("I teach people");
        }
    }
}

