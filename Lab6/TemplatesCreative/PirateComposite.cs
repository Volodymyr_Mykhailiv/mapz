﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplatesCreative
{
    public interface IPirateComponent
    {
        string ShowInfo();
    }


    public class PirateLeaf: IPirateComponent
    {
        private Guid Id { get; set; }
        private string Name { get;  set; }
        private int Money { get; set; }
        private Guid RankId { get;  set; }
        private string RankName { get; set; }

        private readonly bool IsCaptain;
        private string Boat {  get; set; }
        public PirateLeaf(string name, Rank rank, int money)
        {
            Id = Guid.NewGuid();
            Name = name;
            RankId = rank.Id;
            RankName = rank.RankName;
            Money = money;
            IsCaptain = rank.RankName == PirateRank.Captain.ToString();
        }
        public PirateLeaf(string name, Rank rank, int money, string boat)
        {
            Id = Guid.NewGuid();
            Name = name;
            RankId = rank.Id;
            RankName = rank.RankName;
            Money = money;
            IsCaptain = rank.RankName == PirateRank.Captain.ToString();
            Boat = boat;
        }

        public string ShowInfo()
        {
            return $"Boat: {Boat}, Name: {Name}, Rank Name: {RankName},  Money: {Money}\n";
        }
    }

    public class PirateBranch: IPirateComponent
    {
        private List<IPirateComponent> children = new List<IPirateComponent>();

        public void AddComponent(IPirateComponent component)
        {
            children.Add(component);
        }

        public void RemoveComponent(IPirateComponent component)
        {
            children.Remove(component);
        }

        public List<IPirateComponent> GetComponents()
        {
            return children;
        }
       
        public string ShowInfo()
        {
            string result = $"Team\n";
            
            foreach(IPirateComponent component in children)
            {
                result += component.ShowInfo();
            }
            return result;
        }
    }





    public class Rank
    {
        public Guid Id { get; private set; }
        public string RankName { get; private set; }
        public Rank(PirateRank rank)
        {
            RankName = rank.ToString();
            Id = Guid.NewGuid();
        }
        public Rank(string name)
        {
            RankName = name;
            Id = Guid.NewGuid();
        }
    }

    public enum PirateRank
    {
        Captain,
        Quarternaster,
        Boatswain,
        Navigator,
        CabinBoy,
        Cook
    }
}
