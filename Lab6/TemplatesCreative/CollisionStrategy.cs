﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplatesCreative
{
    public interface ICollision
    {
        void CalculateDamage(ref int HP);
    }

    public class FatalCollision : ICollision
    {
        public void CalculateDamage(ref int HP)
        {
            // Console.WriteLine("Відняти все здоров'я, гра закінчилась ");
            HP -= 100;
        }
    }

    public class HalfFatalCollision : ICollision
    {
        public void CalculateDamage(ref int HP)
        {
            //Console.WriteLine("Відняти половину здоров'я, ще є 1 спроба");
            HP -= 50;
        }
    }

    public class ShipCollision
    {
        private ICollision collision;
        public void SetCollision(ICollision collision)
        {
            this.collision = collision;
        }

        public void HandleCollision(ref int HP)
        {
            if (collision != null)
            {
                collision.CalculateDamage(ref HP);
            }
        }
    }

}
