﻿using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TemplatesCreative;

namespace TemplatesCreative
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Point> allPositions = new List<Point>();
        private List<Point> freePositions = new List<Point>();
        public MainWindow()
        {

            InitializeComponent();
            InitializePositions(canvas, 100, 100);
           

            Loaded += MainWindow_Loaded;
            PreviewKeyDown += Window_PreviewKeyDown; // Додати обробник 
        }
        private void InitializePositions(Canvas canvas, double skyImageWidth, double skyImageHeight)
        {
            if (allPositions.Count == 0)
            {
                // Коригуємо обчислення кількості рядків і стовпців
                int rows = (int)(750 / skyImageHeight);
                int cols = (int)(700 / skyImageWidth);

                for (int row = 2; row < rows; row++)
                {
                    for (int col = 0; col < cols; col++)
                    {
                        double left = col * skyImageWidth;
                        double top = row * skyImageHeight;
                        Point position = new Point(left, top);
                        allPositions.Add(position);
                        freePositions.Add(position);
                    }
                }
            }
        }


        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            
            GameMap.Instance.InitializeGameMap(canvas);
        }
        private IShipFactory GalleonFactory = new Galleon();
        private IShipFactory FrigateFactory = new Frigate();

        public Image ShipImage;
        private Treasure treasure;
        private Obstacle obstacle;
        private ShipMovement movement;
        private IBoat galleonBoat;
        private IBoat frigateBoat;
        private Bonus bonus;

        private bool isCreateFactoryClicked = false;
        private void CreateGalleon_Click(object sender, RoutedEventArgs e)
        {
            if (!isCreateFactoryClicked)
            {
                HP = 100;
                HPTextBlock.Text = HP.ToString();
                obstacle = new Obstacle(100);
                obstacle.Generate(canvas, ref allPositions, ref freePositions);
                treasure = new Treasure(10);
                treasure.Generate(canvas, ref allPositions, ref freePositions);

                /////////////
                if (galleonBoat == null)
                {
                    galleonBoat = GalleonFactory.CreateBoat();
                }

                galleonBoat.Sail(canvas, ref allPositions, ref freePositions);
                ShipImage = galleonBoat.GetShipImage();
                movement = new ShipMovement(canvas, ShipImage);

                //////////
                bonus = new SimpleBonus();
                isCreateFactoryClicked = true;
            }
        }

        private void CreateFrigate_Click(object sender, RoutedEventArgs e)
        {
            if (!isCreateFactoryClicked)
            {
                HP = 100;
                HPTextBlock.Text = HP.ToString();
                obstacle = new Obstacle(100);
                obstacle.Generate(canvas, ref allPositions, ref freePositions);
                treasure = new Treasure(10);
                treasure.Generate(canvas, ref allPositions, ref freePositions);

                //////////////
                if (frigateBoat == null)
                {
                    frigateBoat = FrigateFactory.CreateBoat();
                }

                frigateBoat.Sail(canvas, ref allPositions, ref freePositions);
                ShipImage = frigateBoat.GetShipImage(); 
                movement = new ShipMovement(canvas, ShipImage);

                //////////
                bonus = new SimpleBonus();
                isCreateFactoryClicked = true;
            }

        }

        public static List<Rank> Ranks = new()
       {
           new Rank(PirateRank.Captain),
           new Rank(PirateRank.Quarternaster),
           new Rank(PirateRank.Boatswain),
           new Rank(PirateRank.Navigator),
           new Rank(PirateRank.CabinBoy),
           new Rank(PirateRank.Cook),
           new Rank("Cannoneer")
      };

        private void ShowInfoButton_Click(object sender, RoutedEventArgs e)
        {
            var Pirates = new List<IPirateComponent>
            {
                new PirateLeaf("Anne Bonny", Ranks[5], 13, "Frigate"),
                new PirateLeaf("Calico Jack ", Ranks[1], 19, "Frigate"),
                new PirateLeaf("Blackbeard", Ranks[0], 99, "Galleon"),
                new PirateLeaf("William Kidd", Ranks[3], 18, "Galleon"),
                new PirateLeaf("Jean Lafitte", Ranks[2], 33, "Galleon")
            };

            PirateBranch FrigateBranch = new PirateBranch();
            FrigateBranch.AddComponent(Pirates[0]);
            FrigateBranch.AddComponent(Pirates[1]);
            PirateBranch GalleonBranch = new PirateBranch();
            GalleonBranch.AddComponent(Pirates[2]);
            GalleonBranch.AddComponent(Pirates[3]);
            GalleonBranch.AddComponent(Pirates[4]);
            GalleonBranch.AddComponent(FrigateBranch);
            // InfoTextBlock.Text = tree.ShowInfo();
            MessageBox.Show(GalleonBranch.ShowInfo());
        }


        private Bonus improveBonus;
        private ShipCollision collision;

        private int HP = 100;


        public void CheckPositions()
        {
            Point ShipPoint = movement.GetShipCurrentPossition();
            Point TreasurePoint = treasure.GetTreasurePosition();
            Point ObstraclePoint = obstacle.GetEnemyShipPosition();

            if(ShipPoint.X == ObstraclePoint.X && ShipPoint.Y == ObstraclePoint.Y)
            {
                if (collision == null)
                {
                    collision = new ShipCollision();
                    collision.SetCollision(new HalfFatalCollision());
                }
                if (collision != null)
                {
                    collision.HandleCollision(ref HP);
                    HPTextBlock.Text = HP.ToString();
                }
                if(isAttackCollision)
                {
                    bonus.Balance += 50;
                    bonus.Supplies += 10;
                    SuppliesTextBlock.Text = bonus.Supplies.ToString();
                    BalanceTextBlock.Text = bonus.Balance.ToString();
                }
                if(isDefenseCollision)
                {
                    HP += 25;
                    HPTextBlock.Text = HP.ToString();   
                }


                if (HP == 0)
                {
                    SuppliesTextBlock.Text = "0";
                    BalanceTextBlock.Text = "0";
                    canvas.Children.Remove(ShipImage);
                    canvas.Children.Remove(obstacle.EnemyShipImage);
                    canvas.Children.Remove(treasure.TreasureImage);
                    MessageBox.Show("You lost", "Game Over", MessageBoxButton.OK, MessageBoxImage.Error);
                    isCreateFactoryClicked = false;
                }

            }

            if (ShipPoint.X == TreasurePoint.X && ShipPoint.Y == TreasurePoint.Y)
            { 
                if(bonus.Balance % 50 == 0 && bonus.Balance != 0)
                {
                    if (improveBonus == null) 
                    {
                        improveBonus = new ImprovedBonus(bonus); 
                    }
                   
                    improveBonus.CalculateBonus(); // Виконуємо обчислення покращеного бонусу
                  
                    SuppliesTextBlock.Text = improveBonus.Supplies.ToString(); 
                }
                else
                {
                    bonus.CalculateBonus();
                }
                treasure.ChangeTreasurePosition();
                BalanceTextBlock.Text = bonus.Balance.ToString();
            }
        }

        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Up:
                    movement.MoveUp();
                    CheckPositions();
                    break;
                case Key.Down:
                   movement.MoveDown();
                    CheckPositions();
                    break;
                case Key.Left:
                   movement.MoveLeft();
                    CheckPositions();
                    break;
                case Key.Right:
                   movement.MoveRight();
                    CheckPositions();
                    break;
                default:
                    break;
            }
        }

        private bool isAttackCollision = false;
        private bool isDefenseCollision = false;
        private void PrapareAttack_Click(object sender, RoutedEventArgs e)
        {
            ShipMode attackShip = new AttackShipMode();
            string attackShipPreparation = attackShip.PrepareForAction();
            MessageBox.Show(attackShipPreparation);
            isAttackCollision = true;
            isDefenseCollision = false;
           
        }
        private void PrepareDefense_Click(object sender, RoutedEventArgs e)
        {
            ShipMode defenseShip = new DefenseShipMode();
            string defenseShipPreparation = defenseShip.PrepareForAction();
            MessageBox.Show(defenseShipPreparation);
            
            isDefenseCollision = true;
            isAttackCollision = false;
           
        }
    }
}


