﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplatesCreative
{
    abstract class ShipMode
    {
        public string PrepareForAction()
        {
            return RepairShip() + "\n" + LoadSupplies() + "\n" + SetShipMode();
        }

        // Метод для ремонту корабля
        protected virtual string RepairShip()
        {
            return "Ship repaired.";
        }

        // Метод для завантаження припасів
        protected virtual string LoadSupplies()
        {
            return "Supplies loaded.";
        }

        protected abstract string SetShipMode();
    }

    // Клас для підготовки корабля до атаки
    class AttackShipMode : ShipMode
    {
        protected override string SetShipMode()
        {
           
            return "Ship is ready to attack.";
            
        }
    }

    // Клас для підготовки корабля до оборони
    class DefenseShipMode : ShipMode
    {
        protected override string SetShipMode()
        {
           
            return "Ship in defensive position.";
        }
    }

}
