﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
namespace TemplatesCreative
{
    public class GameArea
    {
      public int Width {  get; private set; }
      public int Height { get; private set; }

        public GameArea(int width, int height)
        {
            Width = width;
            Height = height;
        }

        public GameArea()
        {
           
        }

        public bool IsValidPosition(int x, int y)
        {
            return x >= 0 && x <= Width && y >= 200 && y <= Height;
        }
    }
    public class Ship
    {
        public int X { get; private set; }
        public int Y { get; private set; }
        Image ShipImage { get; set; }

        public Ship(int x, int y, Image shipImage)
        {
            X = x;
            Y = y;
            ShipImage = shipImage;
        }

        public Ship() 
        { 

        }
         
        public void MoveUp()
        {
            Y -= 100;
            Canvas.SetTop(ShipImage, Y);
        }

        public void MoveDown()
        {
            Y += 100;
            Canvas.SetTop(ShipImage, Y);
        }
        public void MoveLeft()
        {
            X -= 100;
            Canvas.SetLeft(ShipImage, X);
        }
        public void MoveRight() 
        { 
            X += 100;
            Canvas.SetLeft(ShipImage, X);
        }
    }

    public class ShipMovement
    {
        private readonly Ship _ship;
        private readonly GameArea _map;

        public ShipMovement(Ship ship, GameArea map)
        {
            _ship = ship;
            _map = map;
        }

        public ShipMovement(Canvas canvas, Image image)
        {
            _ship = new Ship(0, 200, image);
            _map = new GameArea((int)canvas.ActualWidth, (int)canvas.ActualHeight);
        }

        public Point GetShipCurrentPossition()
        {
            return new Point(_ship.X, _ship.Y);
        }
        public void MoveUp()
        {
            if (_map.IsValidPosition(_ship.X, _ship.Y - 100))
            {
               _ship.MoveUp();
                Console.WriteLine($"Ship moved up to position ({_ship.X}, {_ship.Y})");
            }
           
        }
        public void MoveDown()
        {
            if (_map.IsValidPosition(_ship.X, _ship.Y + 100))
            {
                _ship.MoveDown();
                Console.WriteLine($"Ship moved down to position ({_ship.X}, {_ship.Y})");
            }
           
        }

        public void MoveLeft()
        {
            if (_map.IsValidPosition(_ship.X - 100, _ship.Y))
            {
               _ship.MoveLeft();
                Console.WriteLine($"Ship moved left to position ({_ship.X} ,  {_ship.Y})");
            }
          
        }

        public void MoveRight()
        {
            if (_map.IsValidPosition(_ship.X + 100, _ship.Y))
            {
                _ship.MoveRight();  
                Console.WriteLine($"Ship moved right to position ({_ship.X} ,  {_ship.Y})");
            }
           
        }
    }

}
