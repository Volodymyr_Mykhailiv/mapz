﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
namespace TemplatesCreative
{
  public interface IMapElement
    {
        IMapElement Clone();
    }

    public class Obstacle : IMapElement
    {
        public int Size { get; set; }

        public int X { get; private set; } = 300;
        public int Y { get; private set; } = 400;
        public Image EnemyShipImage { get; private set; }


        public Obstacle(int size)
        {
            Size = size;
        }

        public void Generate(Canvas canvas, ref List<Point> allPositions, ref List<Point> freePositions)
        {

            string EnemySpipImagePath = ".\\Images\\EnemyShip.png";
            BitmapImage EnemySpipBitmap = new BitmapImage(new Uri(EnemySpipImagePath, UriKind.Relative));

            // Розмір зображення "enemySpip"
            double EnemySpipImageWidth = 100; // Ширина зображення рівна ширині Canvas
            double EnemySpipImageHeight = 100;

            // Початкові координати скарбу

            // Створення Image для "Treasure"
            Image enemySpipImage = new Image();
            enemySpipImage.Source = EnemySpipBitmap;
            enemySpipImage.Width = EnemySpipImageWidth;
            enemySpipImage.Height = EnemySpipImageHeight;
            Canvas.SetLeft(enemySpipImage, X);
            Canvas.SetTop(enemySpipImage, Y);
            canvas.Children.Add(enemySpipImage);
            EnemyShipImage = enemySpipImage;


        }

        public Point GetEnemyShipPosition()
        {
            return new Point(X, Y);
        }

        public IMapElement Clone()
        {
            return new Obstacle(Size);
        }
    }

    public class Treasure : IMapElement
    {
        public int Value { get; set; }

        public int X { get; private set; } = 300;
        public int Y { get; private set; } = 500;
        public Image TreasureImage { get; private set; }
        public Treasure(int value)
        {
            Value = value;
           
        }
     
        public IMapElement Clone()
        {
            return new Treasure(Value);
        }

       public  void Generate(Canvas canvas, ref List<Point> allPositions, ref List<Point> freePositions)
        {
           
            string TreasureImagePath = ".\\Images\\treasure.png";
            BitmapImage treasureBitmap = new BitmapImage(new Uri(TreasureImagePath, UriKind.Relative));

            // Розмір зображення "treasure"
            double TreasureImageWidth = 100; // Ширина зображення рівна ширині Canvas
            double TreasureImageHeight = 100;

            // Початкові координати скарбу
          
            // Створення Image для "Treasure"
            Image treasureImage = new Image();
            treasureImage.Source = treasureBitmap;
            treasureImage.Width = TreasureImageWidth;
            treasureImage.Height = TreasureImageHeight;
            Canvas.SetLeft(treasureImage, X);
            Canvas.SetTop(treasureImage, Y);
            canvas.Children.Add(treasureImage);
            TreasureImage = treasureImage;

            
        }

        public void ChangeTreasurePosition()
        {
            Random random = new Random();

            do
            {
                X = random.Next(0, 7) * 100;
                Y = random.Next(2, 7) * 100;
            }
            while (X == 300 && Y == 400);

            Canvas.SetTop(TreasureImage, Y);
            Canvas.SetLeft(TreasureImage, X);
        }

        public Point GetTreasurePosition()
        {
            return new Point(X, Y);
        }
    }

}
