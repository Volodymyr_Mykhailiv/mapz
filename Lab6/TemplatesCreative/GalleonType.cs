﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace TemplatesCreative
{
    public class Galleon : IShipFactory
    {
        public IBoat CreateBoat()
        {
            return new GalleonBoat();
        }

        public ICrew CreateCrew()
        {
            return new GalleonCrew();
        }

        public IWeapon CreateWeapon()
        {
            return new GalleonWeapon();
        }

        public ISupplies CreateSupplies()
        {
            return new GalleonSupplies();
        }
    }




    // конкретні компоненти типу корабля


    public class GalleonBoat : IBoat
    {



        //public void Sail(Canvas canvas, ref List<Point> allPositions, ref List<Point> freePositions)
        //{
        //    // Шлях до зображення "sky"


        //    string galleonImagePath = ".\\Images\\піратК.png";
        //    BitmapImage galleonBitmap = new BitmapImage(new Uri(galleonImagePath, UriKind.Relative));

        //    // Розмір зображення "sky"
        //    double galleonImageWidth = 100; // Ширина зображення рівна ширині Canvas
        //    double galleonImageHeight = 100; 

        //    // Випадковим чином перемішати список вільних позицій
        //    Random random = new Random();
        //    freePositions = freePositions.OrderBy(p => random.Next()).ToList();

        //    // Список зайнятих позицій
        //    List<Point> occupiedPositions = new List<Point>();

        //    // Проходження по перемішаному списку позицій і вставка зображення в перший вільний сектор
        //    List<Point> positionsToRemove = new List<Point>(); // Список позицій для видалення

        //    foreach (Point position in freePositions)
        //    {
        //        // Перевірка, чи позиція вільна
        //        bool positionOccupied = occupiedPositions.Any(p =>
        //            Math.Abs(p.X - position.X) < galleonImageWidth &&
        //            Math.Abs(p.Y - position.Y) < galleonImageHeight);

        //        if (!positionOccupied)
        //        {
        //            // Створення Image для "sky"
        //            Image galleonImage = new Image();
        //            galleonImage.Source = galleonBitmap;
        //            galleonImage.Width = galleonImageWidth;
        //            galleonImage.Height = galleonImageHeight;
        //            Canvas.SetLeft(galleonImage, position.X);
        //            Canvas.SetTop(galleonImage, position.Y);
        //            canvas.Children.Add(galleonImage);

        //            // Додавання позиції до списку зайнятих позицій
        //            occupiedPositions.Add(position);

        //            positionsToRemove.Add(position); // Додати позицію до списку для видалення

        //            break; // Вставлено зображення, виходимо з циклу
        //        }
        //    }

        //    // Видалення зайнятих позицій зі списку вільних позицій
        //    foreach (var pos in positionsToRemove)
        //    {
        //        freePositions.Remove(pos);
        //    }
        //    if (freePositions.Count == 0)
        //    {
        //        MessageBox.Show("All poitions are occupied");
        //    }
        //}

        public int StartX = 0;
        public int StartY = 200;
        public Image GalleonImage;
        
        public void Sail(Canvas canvas, ref List<Point> allPositions, ref List<Point> freePositions)
        {
            // Шлях до зображення "Galleon"


            string galleonImagePath = ".\\Images\\Galleon.png";
            BitmapImage galleonBitmap = new BitmapImage(new Uri(galleonImagePath, UriKind.Relative));

            // Розмір зображення "sky"
            double galleonImageWidth = 100; // Ширина зображення рівна ширині Canvas
            double galleonImageHeight = 100;

            // Початкові координати корабля

            // Створення Image для "Galleon"
             Image galleonImage = new Image();
            galleonImage.Source = galleonBitmap;
            galleonImage.Width = galleonImageWidth;
            galleonImage.Height = galleonImageHeight;
            Canvas.SetLeft(galleonImage, StartX);
            Canvas.SetTop(galleonImage, StartY);
            galleonImage.Name = "GalleonImage";
            canvas.Children.Add(galleonImage);
            GalleonImage = galleonImage;

        }

        public Image GetShipImage()
        {
            return GalleonImage;
        }





        public void Navigate()
        {
            Console.WriteLine("Go to west Galleon");
        }
    }


    public class GalleonCrew : ICrew
    {
        public void AssignRole()
        {
            Console.WriteLine("You are matros");
        }

        public void AddCrewMember()
        {
            Console.WriteLine("new member to Galleon");
        }

        public void RemoveCrewMember()
        {
            Console.WriteLine("sorry you were dalited from Galleon");
        }
    }

    public class GalleonWeapon : IWeapon
    {
        public void Fire()
        {
            Console.WriteLine("тидищ Galleon");
        }
        public void Reload()
        {
            Console.WriteLine("reload weapon in Galleon");
        }
    }

    public class GalleonSupplies : ISupplies
    {
        public void ConsumeSupply()
        {
            Console.WriteLine("Am am am am aaaam am Galleon");
        }
        public int CheckSupplyLevel()
        {
            return 100;
        }
        public void AddSupply()
        {
            Console.WriteLine("Add bannanas to Galleon");
        }

    }
}
