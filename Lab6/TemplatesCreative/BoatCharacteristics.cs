﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace TemplatesCreative
{

    public interface IBoat
    {
        void Sail(Canvas canvas, ref List<Point> allPositions, ref List<Point> freePositions);
        void Navigate();
        Image GetShipImage();
    }
    public interface ICrew
    {
        void AssignRole();
        void AddCrewMember();
        void RemoveCrewMember();
    }
    public interface IWeapon
    {
        void Fire();
        void Reload();
    }

    public interface ISupplies
    {
        void ConsumeSupply();
        int CheckSupplyLevel();
        void AddSupply();
    }

}
