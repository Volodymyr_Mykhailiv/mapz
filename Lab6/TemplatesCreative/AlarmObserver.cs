﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplatesCreative
{
    public interface IObserver
    {
        void Update();
    }

    public interface IObserved
    {
        void AddObserver(IObserver observer);
        void RemoveObserver(IObserver observer);
        void NotifyObservers();
    }

    public class AlarmOfAttack : IObserved
    {
        private List<IObserver> observers = new List<IObserver>();

        public void AddObserver(IObserver observer)
        {
            observers.Add(observer);
        }

        public void RemoveObserver(IObserver observer)
        {
            observers.Remove(observer);
        }

        public void NotifyObservers()
        {
            foreach (var observer in observers)
            {
                observer.Update();
            }
        }

        public void PrepareAttack()
        {
            Console.WriteLine("Готується атака на корабель");
            NotifyObservers();
        }
    }

    public class Pirate : IObserver
    {
        private string Name { get; set; }

        public Pirate(string name)
        {
            Name = name;
        }

        public void Update()
        {
            Console.WriteLine($"{Name}: Капітан говорить що готується напад");
        }
    }
}
